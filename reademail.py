from googleapiclient  import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools
# 
# pip install apiclient
# pip install googleapiclient
# pip install --upgrade google-api-python-client
# pip install oauth2client
# pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib
# pip install pandas
# pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib
# pip3 install google-auth-oauthlib



import os
import httplib2
import email
from googleapiclient.discovery import BatchHttpRequest
import pandas as pd
import base64
#from bs4 import BeautifulSoup
import re
import datetime
from datetime import date, timedelta
from googleapiclient.errors import HttpError
from google_auth_oauthlib.flow import InstalledAppFlow
from google.oauth2.credentials import Credentials
from google.auth.transport.requests import Request
from googleapiclient.discovery import build

def getsibject():
  SCOPES = ["https://mail.google.com/"]
  creds = None
  if os.path.exists("/home/rstudio/emailreport/token.json"):
     creds = Credentials.from_authorized_user_file("/home/rstudio/emailreport/token.json", SCOPES)

  if not creds or not creds.valid:
     if creds and creds.expired and creds.refresh_token:
       creds.refresh(Request())
     else:
       flow = InstalledAppFlow.from_client_secrets_file(
            "/home/rstudio/emailreport/credentials.json", SCOPES
        )
       creds = flow.run_local_server(port=0)
   
     with open("/home/rstudio/emailreport/token.json", "w") as token:
        token.write(creds.to_json())
  return creds

def labels():

  try:
   creds = getsibject()  
   # Call the Gmail API
   service = build("gmail", "v1", credentials=creds)
   results = service.users().labels().list(userId="me").execute()
   labels = results.get("labels", [])

   if not labels:
     print("No labels found.")
     

   names = []
   ids = []
   for label in labels:
     name = label["name"]
     id = label["id"]
     names.append(name)
     ids.append(id)
   
   names = pd.DataFrame(names)
   ids = pd.DataFrame(ids)
   
   result_concat = pd.concat([names, ids], axis=1)   
   return result_concat
 
  except HttpError as error:
    # TODO(developer) - Handle errors from gmail API.
      print(f"An error occurred: {error}")



def getsubjectlabel():
  try:
   creds = getsibject() 
   service = build("gmail", "v1", credentials=creds)
   #results = service.users().messages().list(userId='me', maxResults=20000).execute()
   result = service.users().messages().list(userId='me', maxResults=600).execute()
   
   messages = results.get('messages', [])

   if not messages:
          print('No se encontraron mensajes.')
   else:
    subjects = []
    labels = []
    dates =[]
    for message in messages:
        msg = service.users().messages().get(userId='me', id=message['id'], format='full').execute()
        headers = msg['payload']['headers']
        subject = next((header['value'] for header in headers if header['name'] == 'Subject'), None)
        date = next((header['value'] for header in headers if header['name'] == 'Date'), None)
        label = msg["labelIds"]
        if label:
            labels.append(label)
        if subject:
            subjects.append(subject)
        if date:
            dates.append(date)

   lbs = pd.DataFrame(labels)
   sbj = pd.DataFrame(subjects)
   dtd = pd.DataFrame(dates)
   result_concat = pd.concat([lbs, sbj], axis=1)
   salida =pd.concat([result_concat, dtd], axis=1)

   return salida
  except HttpError as error:
    # Handle errors from gmail API.
      print(f"An error occurred: {error}")



def getsubjectlabeltime(query,labelx):
  try:
   creds = getsibject() 
   service = build("gmail", "v1", credentials=creds)
   results = service.users().messages().list(userId='me',labelIds=labelx, q=query).execute()
   messages = results.get('messages', [])

   if not messages:
          print('No se encontraron mensajes.')
          salida = pd.DataFrame()

   else:
    subjects = []
    labels = []
    dates = []
    des = []
    for message in messages:
        msg = service.users().messages().get(userId='me', id=message['id'], format='full').execute()
        headers = msg['payload']['headers']
        subject = next((header['value'] for header in headers if header['name'] == 'Subject'), None)
        de = next((header['value'] for header in headers if header['name'] == 'From'), None)
        date = next((header['value'] for header in headers if header['name'] == 'Date'), None)
        label = msg["labelIds"]
        if label:
            labels.append(labelx)
        if subject:
            subjects.append(subject)
        if date:
            dates.append(date)
        if de:
            des.append(de)


    lbs = pd.DataFrame(labels)
    sbj = pd.DataFrame(subjects)
    dtd = pd.DataFrame(dates)
    frd = pd.DataFrame(des)
    result_concat = pd.concat([lbs, sbj], axis=1)
    result_concat2 =pd.concat([result_concat, dtd], axis=1)
    salida =pd.concat([result_concat2, frd], axis=1)
   return salida
  except HttpError as error:
    # Handle errors from gmail API.
      print(f"An error occurred: {error}")


def detfinalsubject(labelx):
  end = date.today()
  salida = pd.DataFrame()
  for i in range(0,160):
    start = end - timedelta(7)
    query = "before: {0} after: {1}".format(end.strftime('%Y/%m/%d'),start.strftime('%Y/%m/%d'))
    print(query)
    oneday = getsubjectlabeltime(query, "INBOX")
    print(i)
    end = start 
    salida = pd.concat([salida, oneday])
  return salida

