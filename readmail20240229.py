import apiclient 
import oauth2client
from oauth2client import client
from oauth2client import tools
import os
import httplib2
import email
from googleapiclient.http import BatchHttpRequest
import base64
#from bs4 import BeautifulSoup
import re
import datetime
from datetime import date, timedelta
import pandas as pd
from httplib2 import Http
from urllib.error import HTTPError
from google_auth_oauthlib.flow import Flow
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient import discovery
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError



def getsibject():
  SCOPES = ["https://mail.google.com/"]
  creds = None
  if os.path.exists("/reports/token.json"):
     creds = Credentials.from_authorized_user_file("/reports/token.json", SCOPES)

  if not creds or not creds.valid:
     if creds and creds.expired and creds.refresh_token:
       creds.refresh(Request())
     else:
       flow = InstalledAppFlow.from_client_secrets_file(
            "/reports/credentials.json", SCOPES
        )
       creds = flow.run_local_server(port=0)
   
     with open("/reports/token.json", "w") as token:
        token.write(creds.to_json())
  return creds

def labels():

  try:
   creds = getsibject()  
   # Call the Gmail API
   service = build("gmail", "v1", credentials=creds)
   results = service.users().labels().list(userId="me").execute()
   labels = results.get("labels", [])

   if not labels:
     print("No labels found.")
     

   names = []
   ids = []
   for label in labels:
     name = label["name"]
     id = label["id"]
     names.append(name)
     ids.append(id)
   
   names = pd.DataFrame(names)
   ids = pd.DataFrame(ids)
   
   result_concat = pd.concat([names, ids], axis=1)   
   return result_concat
 
  except HttpError as error:
    # TODO(developer) - Handle errors from gmail API.
      print(f"An error occurred: {error}")



def getsubjectlabel():
  try:
   creds = getsibject() 
   service = build("gmail", "v1", credentials=creds)
   #results = service.users().messages().list(userId='me', maxResults=20000).execute()
   result = service.users().messages().list(userId='me', maxResults=600).execute()
   
   messages = results.get('messages', [])

   if not messages:
          print('No se encontraron mensajes.')
   else:
    subjects = []
    labels = []
    dates =[]
    for message in messages:
        msg = service.users().messages().get(userId='me', id=message['id'], format='full').execute()
        headers = msg['payload']['headers']
        subject = next((header['value'] for header in headers if header['name'] == 'Subject'), None)
        date = next((header['value'] for header in headers if header['name'] == 'Date'), None)
        label = msg["labelIds"]
        if label:
            labels.append(label)
        if subject:
            subjects.append(subject)
        if date:
            dates.append(date)

   lbs = pd.DataFrame(labels)
   sbj = pd.DataFrame(subjects)
   dtd = pd.DataFrame(dates)
   result_concat = pd.concat([lbs, sbj], axis=1)
   salida =pd.concat([result_concat, dtd], axis=1)

   return salida
  except HttpError as error:
    # Handle errors from gmail API.
      print(f"An error occurred: {error}")



def getsubjectlabeltime(query,labelx):
  try:
   creds = getsibject() 
   service = build("gmail", "v1", credentials=creds)
   results = service.users().messages().list(userId='me',labelIds=labelx, q=query).execute()
   messages = results.get('messages', [])

   if not messages:
          print('No se encontraron mensajes.')
          salida = pd.DataFrame()

   else:
    subjects = []
    labels = []
    dates = []
    des = []
    for message in messages:
        msg = service.users().messages().get(userId='me', id=message['id'], format='full').execute()
        headers = msg['payload']['headers']
        subject = next((header['value'] for header in headers if header['name'] == 'Subject'), None)
        de = next((header['value'] for header in headers if header['name'] == 'From'), None)
        date = next((header['value'] for header in headers if header['name'] == 'Date'), None)
        label = msg["labelIds"]
        if label:
            labels.append(labelx)
        if subject:
            subjects.append(subject)
        if date:
            dates.append(date)
        if de:
            des.append(de)


    lbs = pd.DataFrame(labels)
    sbj = pd.DataFrame(subjects)
    dtd = pd.DataFrame(dates)
    frd = pd.DataFrame(des)
    result_concat = pd.concat([lbs, sbj], axis=1)
    result_concat2 =pd.concat([result_concat, dtd], axis=1)
    salida =pd.concat([result_concat2, frd], axis=1)
   return salida
  except HttpError as error:
    # Handle errors from gmail API.
      print(f"An error occurred: {error}")


def detfinalsubject(labelx):
  end = date.today()
  salida = pd.DataFrame()
  for i in range(0,160):
    start = end - timedelta(7)
    query = "before: {0} after: {1}".format(end.strftime('%Y/%m/%d'),start.strftime('%Y/%m/%d'))
    print(query)
    oneday = getsubjectlabeltime(query, "INBOX")
    print(i)
    end = start 
    salida = pd.concat([salida, oneday])
  return salida

