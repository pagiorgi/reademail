library(reticulate)
library(dplyr)
setwd("/reports")
source_python('reademail.py')
clean <- function(df) {
  #delete re:
  df<-inbox[substr(df$subject, 1, 3)!="Re:", ] 
  df<-df[!grepl("Re:", df$subject), ]
  df<-df[substr(df$subject, 1,3)!="RE:", ]
  #elimino los room mention
  df<-df[!grepl("room mention", df$subject), ]
  #elimino Undelivered Mail Returned to Sender
  df<-df[!grepl("Undelivered Mail Returned to Sender", df$subject), ]
  #elimino Fwd: 
  df<-df[!grepl("Fwd:", df$subject), ]
  
  df
}
catalog <- function(df) {
  df$catalog <-"unknow"
  df$catalog<-ifelse(grepl("Patching",df$subject),"Patching",
                     ifelse(grepl("APPROVAL",df$subject),"APPROVAL",
                            ifelse(grepl("o Action Required",df$subject),"No Action Required",
                                   ifelse(grepl("Maintenance",df$subject),"Maintenance",
                                          ifelse(grepl("Mandatory Urgent Change",df$subject),"Mandatory Urgent Change",
                                                 ifelse(grepl("Planned Maintenance with Downtime",df$subject),"Planned Maintenance with Downtime",
                                                        ifelse(grepl("Planned Maintenance without Downtime",df$subject),"Planned Maintenance without Downtime",
                                                               ifelse(grepl("CaaS maintenance",df$subject),"CaaS maintenance",
                                                                      ifelse(grepl("Roverflow",df$subject),"Roverflow",
                                                                             ifelse(grepl("Automatic access provision",df$subject),"Automatic access provision",
                                                                                    ifelse(grepl("Delivery Status Notification",df$subject),"Delivery Status Notification",
                                                                                           ifelse(grepl("Failed provision CAAS for project",df$subject),"Failed provision CAAS for project",
                                                                                                  ifelse(grepl("Failed relation creation",df$subject),"Failed relation creation",
                                                                                                         ifelse(grepl("Failure",df$subject),"Failure",
                                                                                                                ifelse(grepl("Invitation",df$subject),"Invitation",
                                                                                                                       ifelse(grepl("Join Us",df$subject),"Join Us",
                                                                                                                              ifelse(grepl("Migration",df$subject),"Migration",
                                                                                                                                     ifelse(grepl("P1",df$subject),"P1",
                                                                                                                                            ifelse(grepl("Rancher Operation on node",df$subject),"Rancher Operation on node",
                                                                                                                                                   ifelse(grepl("decom",df$subject),"decommision",
                                                                                                                                                          ifelse(grepl("Returned mail",df$subject),"Returned mail",
                                                                                                                                                                 ifelse(grepl("Change",df$subject),"Change",
                                                                                                                                                                        ifelse(grepl("CHG",df$subject),"Change",
                                                                                                                                                                               ifelse(grepl("RITM",df$subject),"TASK",
                                                                                                                                                                                      ifelse(grepl("SCTAS",df$subject),"Request",
                                                                                                                                                                                             ifelse(grepl("ServiceNow4IT",df$subject),"ServiceNow4IT","client")
                                                                                                                                                                                      )))))))))))))))))))))))))
  df 
}


inbox<-detfinalsubject("INBOX")
names(inbox)<-c("label", "subject", "time", "de")
cleanandcatalog<-catalog(clean(inbox))

time <- function(time) {
  ifelse(nchar(time)>=30,substr(time, 6, 16),
         ifelse(nchar(time)<30,substr(time, 1, 11),"unknow"))
  
}

cleanandcatalog$dia<-time(cleanandcatalog$time)
cleanandcatalog$dia2<-as.Date(cleanandcatalog$dia, "%d %b %Y", tryFormats = c("%d %b %Y"))
cleanandcatalog<-cleanandcatalog[!is.na(cleanandcatalog$dia2),]


catalogde <- function(df) {
  df$catalogde <-"unknow"
  df$catalogde<-ifelse(grepl("global.caas",df$de),"CAAS",
                       ifelse(grepl("SUSE",df$de),"SUSE",
                              ifelse(grepl("IS communications hub",df$de),"IS communications hub",
                                     ifelse(grepl("Roverflow",df$de),"Roverflow",
                                            ifelse(grepl("noreply-apps-scripts-notifications",df$de),"Roverflow",
                                                   ifelse(grepl("GitLab",df$de),"GitLab",
                                                          ifelse(grepl("Roche ServiceNow",df$de),"ServiceNow",
                                                                 "client")
                                                   ))))))
  df
}

pp<-catalogde(cleanandcatalog)

library(dplyr)                                                                                   
#me quedo con los clientes y los serficenow
pp<- pp %>% dplyr::filter(catalogde %in% c("ServiceNow","client" ))

pp<-pp[pp$catalogde %in% c("ServiceNow","client" ), ] 
pp$month<-substr(pp$dia2, 1,7)





### creo el pdf

cleanandcatalog<- cleanandcatalog
pp<-pp

rmarkdown::render(
  
  input = "template.Rmd",
  output_format = "pdf_document",
  output_file = "emails report.pdf",
  output_dir = ".",
  params = list(
    cleanandcatalog= cleanandcatalog,
    pp=pp
  )
  
)


library(googleAuthR)
library(googledrive)

# Autenticación
googleAuthR::gar_auth_service(json = "/home/rstudio/emailreport/credentials.json")
# Ruta al archivo PDF local
ruta_pdf <- "/reports/emails report.pdf"

# Subir el archivo PDF
pdf<-drive_upload(media = ruta_pdf, name = "emails report.pdf", overwrite=T)

path<-paste0("https://drive.google.com/file/d/",pdf$id,"/view?usp=drive_link")


source_python('pyfunction.py')
hook<- Sys.getenv("HOOK")
enviar(path, hook)







